package com.insta.downloader;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;

import io.paperdb.Paper;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        MobileAds.initialize(this);



    }
}

