package com.insta.downloader.model;

import java.io.File;

public class SavedModel {
    File file;
    String file_name;

    public SavedModel(File file, String file_name) {
        this.file = file;
        this.file_name = file_name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }
}

