package com.insta.downloader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.insta.downloader.R;
import com.insta.downloader.model.SavedModel;

import java.io.File;
import java.util.ArrayList;

public class FileListAdapter extends RecyclerView.Adapter<FileListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<SavedModel> fileArrayList;
    private LayoutInflater layoutInflater;
    private FileListClickInterface fileListClickInterface;

    public FileListAdapter(Context context, ArrayList<SavedModel> files, FileListClickInterface fileListClickInterface) {
        this.context = context;
        this.fileArrayList = files;
        this.fileListClickInterface = fileListClickInterface;
    }

    @NonNull
    @Override
    public FileListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.getContext());
        }
        view = layoutInflater.inflate(R.layout.items_file_view, viewGroup, false);
        return new FileListAdapter.ViewHolder(view);

//        return new ViewHolder(DataBindingUtil.inflate(layoutInflater, R.layout.items_file_view, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FileListAdapter.ViewHolder viewHolder, int i) {
        File fileItem = fileArrayList.get(i).getFile();
        viewHolder.tv_fileName.setText(fileArrayList.get(i).getFile_name());
        try {
            String extension = fileItem.getName().substring(fileItem.getName().lastIndexOf("."));
            if (extension.equals(".mp4")) {
                viewHolder.iv_play.setVisibility(View.VISIBLE);
            } else {
                viewHolder.iv_play.setVisibility(View.GONE);
            }
            Glide.with(context)
                    .load(fileItem.getPath())
                    .into(viewHolder.pc);
        } catch (Exception ex) {
        }

        viewHolder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileListClickInterface.getPosition(i, fileItem);
            }
        });
    }


    @Override
    public int getItemCount() {
        return fileArrayList == null ? 0 : fileArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView pc, iv_play;
        RelativeLayout rl_main;
        TextView tv_fileName;


        public ViewHolder(View itemView) {
            super(itemView);
            pc = itemView.findViewById(R.id.pc);
            iv_play = itemView.findViewById(R.id.iv_play);
            rl_main = itemView.findViewById(R.id.rl_main);
            tv_fileName = itemView.findViewById(R.id.tv_fileName);
        }
    }
}
