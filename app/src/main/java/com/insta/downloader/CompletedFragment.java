package com.insta.downloader;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.insta.downloader.adapter.FileListAdapter;
import com.insta.downloader.adapter.FileListClickInterface;
import com.insta.downloader.model.SavedModel;

import java.io.File;
import java.util.ArrayList;

import static com.insta.downloader.Utils.Utils.RootDirectoryInstaShow;


public class CompletedFragment extends Fragment implements FileListClickInterface {
    private FileListAdapter fileListAdapter;
    private ArrayList<SavedModel> fileArrayList;
    private ArrayList<File> fileArrayList_full;
    RecyclerView rv_fileList;
    View view;
SwipeRefreshLayout swiperefresh;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=  inflater.inflate(R.layout.fragment_completed, container, false);

        initViews();
        return view;
    }
    private void initViews(){

        swiperefresh = view.findViewById(R.id.swiperefresh);
        rv_fileList = view.findViewById(R.id.rv_fileList);

        swiperefresh.setOnRefreshListener(() -> {
            getAllFiles();
            swiperefresh.setRefreshing(false);
        });
        getAllFiles();
    }

    private void getAllFiles(){
        fileArrayList = new ArrayList<>();
        fileArrayList_full = new ArrayList<>();
        File[] files = RootDirectoryInstaShow.listFiles();
        if (files!=null) {
            for (File file : files) {

                String name = file.getName();
                if (name==null){
                    name="Insta Video";
                }
                fileArrayList.add(new SavedModel(file,name));
                fileArrayList_full.add(file);
            }
            fileListAdapter = new FileListAdapter(getActivity(), fileArrayList, CompletedFragment.this);
            rv_fileList.setAdapter(fileListAdapter);
        }
    }

    @Override
    public void getPosition(int position, File file) {
        Intent inNext = new Intent(getActivity(), FullViewActivity.class);
        inNext.putExtra("ImageDataFile", fileArrayList_full);
        inNext.putExtra("Position", position);
        getActivity().startActivity(inNext);

    }

}