package com.insta.downloader;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.insta.downloader.Utils.Utils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        BottomNavigationView navView = findViewById(R.id.nav_view_bottom);
        loadFragment(new HomeFragment());
        navView.getMenu().findItem(R.id.navigation_scan).setChecked(true);

        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_scan:
                        loadFragment(new HomeFragment());
                        return true;
                    case R.id.navigation_create:
                        loadFragment(new CompletedFragment());
                        return true;
                    case R.id.insta:
                        Utils.OpenApp(MainActivity.this,"com.instagram.android");
                        return true;

                }
                return false;
            }
        });

    }
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_frame, fragment);
//        transaction.addToBackStack(null);
        transaction.commit();
    }
}