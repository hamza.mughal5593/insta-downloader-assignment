package com.insta.downloader;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.insta.downloader.adapter.ShowImagesAdapter;

import java.io.File;
import java.util.ArrayList;

public class FullViewActivity extends AppCompatActivity {
    private FullViewActivity activity;
    private ArrayList<File> fileArrayList;
    private int Position = 0;
    ShowImagesAdapter showImagesAdapter;

    ViewPager vp_view;
    ImageView im_close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_view);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            fileArrayList= (ArrayList<File>) getIntent().getSerializableExtra("ImageDataFile");
            Position = getIntent().getIntExtra("Position",0);
        }
initViews();

    }
    public void initViews(){

        vp_view=findViewById(R.id.vp_view);
        im_close=findViewById(R.id.im_close);

        showImagesAdapter=new ShowImagesAdapter(this, fileArrayList,FullViewActivity.this);
        vp_view.setAdapter(showImagesAdapter);
        vp_view.setCurrentItem(Position);

        vp_view.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {
                Position=arg0;
                System.out.println("Current position=="+Position);
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
            @Override
            public void onPageScrollStateChanged(int num) {
            }
        });

//        binding.imDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                AlertDialog.Builder ab = new AlertDialog.Builder(activity);
//                ab.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        boolean b=fileArrayList.get(Position).delete();
//                        if (b){
//                            deleteFileAA(Position);
//                        }
//                    }
//                });
//                ab.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        dialog.cancel();
//                    }
//                });
//                AlertDialog alert = ab.create();
//                alert.setTitle(getResources().getString(R.string.do_u_want_to_dlt));
//                alert.show();
//            }
//        });
//        binding.imShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (fileArrayList.get(Position).getName().contains(".mp4")){
//                    Log.d("SSSSS", "onClick: "+fileArrayList.get(Position)+"");
//                    shareVideo(activity,fileArrayList.get(Position).getPath());
//                }else {
//                    shareImage(activity,fileArrayList.get(Position).getPath());
//                }
//            }
//        });
//        binding.imWhatsappShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (fileArrayList.get(Position).getName().contains(".mp4")){
//                    shareImageVideoOnWhatsapp(activity,fileArrayList.get(Position).getPath(),true);
//                }else {
//                    shareImageVideoOnWhatsapp(activity,fileArrayList.get(Position).getPath(),false);
//                }
//            }
//        });
        im_close.setOnClickListener(v -> {
            onBackPressed();
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        activity = this;
    }
}