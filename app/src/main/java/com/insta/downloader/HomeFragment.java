package com.insta.downloader;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.insta.downloader.Utils.CommonClassForAPI;
import com.insta.downloader.Utils.Utils;
import com.insta.downloader.model.Edge;
import com.insta.downloader.model.EdgeSidecarToChildren;
import com.insta.downloader.model.ResponseModel;

import java.io.File;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;

import io.reactivex.observers.DisposableObserver;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;
import static android.content.Context.CLIPBOARD_SERVICE;
import static com.insta.downloader.Utils.Utils.APP_ID;
import static com.insta.downloader.Utils.Utils.APP_SECRT_ID;
import static com.insta.downloader.Utils.Utils.RootDirectoryInsta;
import static com.insta.downloader.Utils.Utils.createFileFolder;
import static com.insta.downloader.Utils.Utils.startDownload;


public class HomeFragment extends Fragment {
TextView tv_paste,login_btn1;
    private ClipboardManager clipBoard;
View view;
    private String VideoUrl;
    private String PhotoUrl;
    CommonClassForAPI commonClassForAPI;
EditText et_text;



    RelativeLayout ad_main2;
    LinearLayout nativead2;
    com.google.android.gms.ads.nativead.NativeAd native_Ad2;
    //    com.facebook.ads.NativeAd fb_nativeAd2;

    ProgressBar progressBar2;
    TextView tv_loading2;
    FrameLayout frameLayout_dialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_home, container, false);
        commonClassForAPI = CommonClassForAPI.getInstance(getActivity());

        initViews();

        ad_main2 = view.findViewById(R.id.ad_main2);
        nativead2 = view.findViewById(R.id.nativead2);
        progressBar2 = view.findViewById(R.id.progressbar2);
        tv_loading2 = view.findViewById(R.id.tv_loading2);
        frameLayout_dialog = view.findViewById(R.id.fl_adplaceholder32);
//        nativeAdLayout_exit = findViewById(R.id.native_ad_container2);
        Boolean status = Utils.isNetworkAvailable2(getActivity());

        if (status) {
            ad_main2.setVisibility(View.VISIBLE);
            nativead2.setVisibility(View.VISIBLE);


                refreshAd2();

        } else {
            ad_main2.setVisibility(View.GONE);
            nativead2.setVisibility(View.GONE);
        }

        return view;
    }

    private void initViews() {
        clipBoard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);



        tv_paste = view.findViewById(R.id.tv_paste);
        login_btn1 = view.findViewById(R.id.login_btn1);
        et_text = view.findViewById(R.id.et_text);




        login_btn1.setOnClickListener(v -> {

            String LL = et_text.getText().toString();
            if (LL.equals("")) {
                Utils.setToast(getActivity(), getResources().getString(R.string.enter_url));
            } else if (!Patterns.WEB_URL.matcher(LL).matches()) {
                Utils.setToast(getActivity(), getResources().getString(R.string.enter_valid_url));
            } else {
                GetInstagramData();
//                showInterstitial();
            }


        });

        tv_paste.setOnClickListener(v -> {
            PasteText();
        });



    }


    private void GetInstagramData() {
        try {
            createFileFolder();
            URL url = new URL(et_text.getText().toString());
            String host = url.getHost();
            Log.e("initViews: ", host);
            if (host.equals("www.instagram.com")) {
                callDownload(et_text.getText().toString());
            } else {
                Utils.setToast(getActivity(), getResources().getString(R.string.enter_valid_url));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void callDownload(String Url) {
        String UrlWithoutQP = getUrlWithoutParameters(Url);
        UrlWithoutQP = UrlWithoutQP + "?__a=1";
        try {
            Utils utils = new Utils(getActivity());
            if (utils.isNetworkAvailable()) {
                if (commonClassForAPI != null) {
                    Utils.showProgressDialog(getActivity());
                    commonClassForAPI.callResult(instaObserver, UrlWithoutQP,
                            "ds_user_id="+APP_ID+"; sessionid="+APP_SECRT_ID);
                }
            } else {
                Utils.setToast(getActivity(), getResources().getString(R.string.no_net_conn));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    private String getUrlWithoutParameters(String url) {
        try {
            URI uri = new URI(url);
            return new URI(uri.getScheme(),
                    uri.getAuthority(),
                    uri.getPath(),
                    null, // Ignore the query part of the input url
                    uri.getFragment()).toString();
        } catch (Exception e) {
            e.printStackTrace();
            Utils.setToast(getActivity(), getResources().getString(R.string.enter_valid_url));
            return "";
        }
    }



    private DisposableObserver<JsonObject> instaObserver = new DisposableObserver<JsonObject>() {
        @Override
        public void onNext(JsonObject versionList) {
            Utils.hideProgressDialog(getActivity());
            try {


                Log.e("onNext: ", versionList.toString());
                Type listType = new TypeToken<ResponseModel>() {
                }.getType();
                ResponseModel responseModel = new Gson().fromJson(versionList.toString(), listType);
                EdgeSidecarToChildren edgeSidecarToChildren = responseModel.getGraphql().getShortcode_media().getEdge_sidecar_to_children();
                if (edgeSidecarToChildren != null) {
                    List<Edge> edgeArrayList = edgeSidecarToChildren.getEdges();
                    for (int i = 0; i < edgeArrayList.size(); i++) {
                        if (edgeArrayList.get(i).getNode().isIs_video()) {
                            VideoUrl = edgeArrayList.get(i).getNode().getVideo_url();
                            startDownload(VideoUrl, RootDirectoryInsta, getActivity(), getVideoFilenameFromURL(VideoUrl));
                            et_text.setText("");
                            VideoUrl = "";

                        } else {
                            PhotoUrl = edgeArrayList.get(i).getNode().getDisplay_resources().get(edgeArrayList.get(i).getNode().getDisplay_resources().size() - 1).getSrc();
                            startDownload(PhotoUrl, RootDirectoryInsta, getActivity(), getImageFilenameFromURL(PhotoUrl));
                            PhotoUrl = "";
                            et_text.setText("");
                        }
                    }
                } else {
                    boolean isVideo = responseModel.getGraphql().getShortcode_media().isIs_video();
                    if (isVideo) {
                        VideoUrl = responseModel.getGraphql().getShortcode_media().getVideo_url();
                        //new DownloadFileFromURL().execute(VideoUrl,getFilenameFromURL(VideoUrl));
                        startDownload(VideoUrl, RootDirectoryInsta, getActivity(), getVideoFilenameFromURL(VideoUrl));
                        VideoUrl = "";
                        et_text.setText("");
                    } else {
                        PhotoUrl = responseModel.getGraphql().getShortcode_media().getDisplay_resources()
                                .get(responseModel.getGraphql().getShortcode_media().getDisplay_resources().size() - 1).getSrc();

                        startDownload(PhotoUrl, RootDirectoryInsta, getActivity(), getImageFilenameFromURL(PhotoUrl));
                        PhotoUrl = "";
                        et_text.setText("");
                        // new DownloadFileFromURL().execute(PhotoUrl,getFilenameFromURL(PhotoUrl));
                    }
                }



            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(Throwable e) {
            Utils.hideProgressDialog(getActivity());
            e.printStackTrace();
        }

        @Override
        public void onComplete() {
            Utils.hideProgressDialog(getActivity());
        }
    };

    public String getImageFilenameFromURL(String url) {
        try {
            return new File(new URL(url).getPath().toString()).getName();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return System.currentTimeMillis() + ".png";
        }
    }
    public String getVideoFilenameFromURL(String url) {
        try {
            return new File(new URL(url).getPath().toString()).getName();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return System.currentTimeMillis() + ".mp4";
        }
    }
    private void PasteText() {
        try {
            et_text.setText("");
            String CopyIntent = getActivity().getIntent().getStringExtra("CopyIntent");
            if (CopyIntent==null)
                CopyIntent="";
            if (CopyIntent.equals("")) {
                if (!(clipBoard.hasPrimaryClip())) {

                } else if (!(clipBoard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {
                    if (clipBoard.getPrimaryClip().getItemAt(0).getText().toString().contains("instagram.com")) {
                        et_text.setText(clipBoard.getPrimaryClip().getItemAt(0).getText().toString());
                    }

                } else {
                    ClipData.Item item = clipBoard.getPrimaryClip().getItemAt(0);
                    if (item.getText().toString().contains("instagram.com")) {
                        et_text.setText(item.getText().toString());
                    }

                }
            } else {
                if (CopyIntent.contains("instagram.com")) {
                    et_text.setText(CopyIntent);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void refreshAd2() {
        AdLoader adLoader = new AdLoader.Builder(getActivity(), getString(R.string.native_advance))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {

                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {
                        progressBar2.setVisibility(View.GONE);
                        tv_loading2.setVisibility(View.GONE);
                        nativead2.setVisibility(View.VISIBLE);

                        if (native_Ad2 != null) {
                            native_Ad2.destroy();
                        }
                        native_Ad2 = nativeAd;

                        NativeAdView adView3 = (NativeAdView) getLayoutInflater().inflate(R.layout.ad_native_full, null);
                        Utils.populateFullAdavanceNative(native_Ad2, adView3);

                        frameLayout_dialog.removeAllViews();


                        frameLayout_dialog.addView(adView3);
                    }
                }).withAdListener(new AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        nativead2.setVisibility(View.GONE);
                        super.onAdFailedToLoad(loadAdError);
                    }
                })
                .withNativeAdOptions(new com.google.android.gms.ads.nativead.NativeAdOptions.Builder().build()).build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }
}